console.log("hello");


/*
	Objects
		 an object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.

	Creating objects using object literal:
		Syntax:
			let objectName = {
					keyA: valueA,
					keyB: valueB
			}

*/




let Student = {
	firstName : "Gabriel",
	lastName : "Guillermo",
	age : 32,
	studentID : "2022-197",
	email : ["gabby961989@gmailcom","gabrieldguillermo@gmail.com"],
	address : {
		street : "125 Ilang-ilang street",
		city :"Tacloban City",
		country :"Philippines"
	}
}

console.log("Result from creating an Object");
console.log(Student)



// Creatin Objects using Constructor Function

/*
	Create a reusable function to create a several Objects that have the same data structure. This is useful for creating multiple instances/copies of an object.


	Syntax:
		function ObjectName(valueA, valueB){
			this.keyA = valueA
			this.keyB = valueB
		}

		let variable = new function ObjectName(valueA, valueB)

		console.log(variable)

			-"this" is a keyword that is used for invoking; in refers to the global object
			- don't forget to add "new" keyword when creating the variables.

*/

function Laptop(name, manufactureDate){
	this.name = name
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Lenovo", 2028);
console.log("Result of creating objects using object constructor:");
console.log(laptop);


let myLaptop = new Laptop("MacBook Air", [2020, 2021]);

console.log("Result of creating objects using object constructor:");
console.log(myLaptop);

// Creating empty object as placeholder

let computer={}
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

myComputer = {
	name: "Asus",
	manufactureDate : 2012
}

console.log(myComputer);

/*MINI ACTIVITY
	- Create an object constructor function to produce 2 objects with 3 key-value pairs
	- Log the 2 new objects in the console and send SS in our GC.

*/

function Car(model, color, manufactureDate){
	this.model = model
	this.color =color
	this.manufactureDate = manufactureDate
}

 let ferrari = new Car("f40","red",2010);
 let toyota= new Car("vios","white",2008);
 console.log(ferrari);
 console.log(toyota);




 /*==============*/
console.group("object delete and reassigning")

 console.log("Result from dot notation: " + myLaptop.name)


  console.log("Result from dot notation: " + myLaptop["name"])


  let cars = {}

cars.name ="Honda Civic"
console.log(cars);

cars["manufacture date"] = 2019;
console.log(cars);

//delete object property
delete cars["manufacture date"];
// cars["manufacture date"]= ' ';
console.log(cars);


// Reassigning object properties
cars.name = "Tesla";
console.log(cars);


cars.name = ["toyota","ferrari"];
console.log(cars);


console.groupEnd("");

console.group("object Methods");

let person = {
	name : "John",
	talk : function(){
		console.log("Hello My Name is " +this.name);
	}
}

person.talk();


person.walk = function () {
	console.log(this.name + "walked 25 steps forward");
}

person.walk()



let Friend = {

firstName:"John",
lastName : "Doe",
address : {
	city : "Austin, Texas",
	country: "US"
},
email:["johnD@gmailcom", "joel2@yahoo.com"],
introduce: function(){
	console.log("Hello My Name is " + this.firstName + " " + this.lastName)
}

}
Friend.introduce()

console.groupEnd();


// Real World Application

/*
	Scenario:
		1. We would like to creat a game that would have sevral pokemon to interact with each other.
		2. every pokemon would have the same sets of stats, properties and functions.

*/


console.group("pokemon");
// Using Object Literals
let myPokemon = {
		name: "Pikachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function() {
			console.log("This pokemon takcled tartetPokemon")
			console.log("tartetPokemon's health is now reduced to targetPokemonHealth")
		},
		faint: function() {
			console.log("Pokemon fainted")
		}

}


// Using Object Constructor
function Pokemon(name, level) {

	// Properties
	this.name = name
	this.level = level
	this.health = 3 *level
	this.attack = level

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled "  + target.name)
		console.log(target.name + "'s health is now reduced " + (target.health - this.attack)) 
	},
	this.faint = function() {
		console.log(this.name + " fainted")
	}
}

let charizard = new Pokemon("Charizard", 12)
let squirtle = new Pokemon("Squirtle", 6)

console.log(charizard)
console.log(squirtle)

charizard.tackle(squirtle)
charizard.tackle(squirtle)

console.groupEnd();



