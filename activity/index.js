
//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:
Part 1: 
    1. Initialize/add the following trainer object properties:
      Name (String)
      Age (Number)
      Pokemon (Array)
      Friends (Object with Array values for properties)
    2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
    3. Access the trainer object properties using dot and square bracket notation.
    4. Invoke/call the trainer talk object method.


*/
//Code Here:

let trainer = {
	name : "Gabby Guillermo",
	age : 18,
	pokemon: ["Mewtwo","Pikachu","Squirtle"],
	friends: {
		japan:["Taguro","Vincent"],
		china:["Eugene", "Alfred"]
	}
}
console.log(trainer);


trainer.talk = function(myPokemon){
		console.log(this.pokemon[myPokemon] + " I choose you!");
	}

console.log("%cResult of adding object method named talk.","color:orange");
console.log(trainer);

console.log("%cResult using dot and square bracket notation.","color:orange");
console.log(trainer.pokemon[2]);

console.log("%cResult of talk method","color:orange");
trainer.talk(1);



/*=======================*/

/*Part 2:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/

function Pokemon(name, level) {

	// Properties
	this.name = name
	this.level = level
	this.health = 3 *level
	this.attack = level

	// Methods
	this.tackle = function(target) {
			console.log(this.name + " tackled "  + target.name)
			console.log(target.name + "'s health is now reduced " + (target.health - this.attack)) 
			target.health = target.health - this.attack;
			
			if (target.health < 5) {
				this.faint(target);
			}
	},
	this.faint = function(target) {
		console.log(target.name + " fainted")
	}
}

let bulbasaur = new Pokemon("bulbasaur", 9)
let mewtwo = new Pokemon("mewtwo", 10)
console.log("%c bulbasaur","color:orange");
console.log(bulbasaur);

console.log("%c mewtwo","color:orange");
console.log(mewtwo);

mewtwo.tackle(bulbasaur);
mewtwo.tackle(bulbasaur);
mewtwo.tackle(bulbasaur);


